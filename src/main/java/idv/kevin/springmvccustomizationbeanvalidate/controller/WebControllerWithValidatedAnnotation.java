package idv.kevin.springmvccustomizationbeanvalidate.controller;

import idv.kevin.springmvccustomizationbeanvalidate.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Email;

@Controller
@Slf4j
@Validated
public class WebControllerWithValidatedAnnotation {

    @GetMapping("/showForm3")
    public String showForm3() {
        return "emailFormViaAjax.html";
    }

    @PostMapping("/validateEmailViaAjax")
    @ResponseBody
    public ResultVO validateEmail(@RequestParam("pEmail") @Email(message = "not allow email type") String pEmail, HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.setAttribute("pEmail", pEmail);

        return new ResultVO().setResult(true).setMessage("operation success !!");
    }

    @GetMapping("/mailEndPage")
    public String showMailEndPage(Model model, HttpServletRequest req) {
        String pEmail = (String) req.getSession().getAttribute("pEmail");
        model.addAttribute("pEmail", pEmail);
        return "mailEndPage";
    }
}
