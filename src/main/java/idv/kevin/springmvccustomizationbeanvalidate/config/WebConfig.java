package idv.kevin.springmvccustomizationbeanvalidate.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//  .resourceChain(false); 這個設定似乎在某些版本的springMVC必須加入,此屬框架的Bug

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/webjars/**", "/css/**", "/js/**")
                .addResourceLocations("/webjars/", "classpath:/static/css/", "classpath:/static/js/")
                .resourceChain(false);
    }


}
