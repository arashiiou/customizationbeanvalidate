package idv.kevin.springmvccustomizationbeanvalidate.vo;

import idv.kevin.springmvccustomizationbeanvalidate.constant.StringConstant;
import idv.kevin.springmvccustomizationbeanvalidate.validate.IdCheck;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

import static idv.kevin.springmvccustomizationbeanvalidate.constant.PatternConstant.*;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class PersonVO {
    @NotBlank(message = StringConstant.NOT_BLANK_CHECK_ERROR_MESSAGE)
    private String pName;
    @NotBlank(message = StringConstant.NOT_BLANK_CHECK_ERROR_MESSAGE)
    @Pattern(regexp = PHONE_PATTERN, message = StringConstant.PHONE_CHECK_ERROR_MESSAGE)
    private String pPhone;
    @NotBlank(message = StringConstant.NOT_BLANK_CHECK_ERROR_MESSAGE)
    @Pattern(regexp = MOBILE_PATTERN, message = StringConstant.MOBILE_CHECK_ERROR_MESSAGE)
    private String pMobile;
    @NotBlank(message = StringConstant.NOT_BLANK_CHECK_ERROR_MESSAGE)
    private String pAddress;
    @NotBlank(message = StringConstant.NOT_BLANK_CHECK_ERROR_MESSAGE)
    @IdCheck
    private String pIdNumber;
    @NotNull(message = StringConstant.NOT_BLANK_CHECK_ERROR_MESSAGE)
    @DateTimeFormat(pattern = BIRTHDAY_PATTERN)
    private Date pBirthday;
    private String pEmail;

    public PersonVO(String pName, String pPhone, String pMobile, String pAddress, String pIdNumber, Date pBirthday, String pEmail) {
        this.pName = pName;
        this.pPhone = pPhone;
        this.pMobile = pMobile;
        this.pAddress = pAddress;
        this.pIdNumber = pIdNumber;
        this.pBirthday = pBirthday;
        this.pEmail = pEmail;
    }

    public PersonVO() {
    }
}
