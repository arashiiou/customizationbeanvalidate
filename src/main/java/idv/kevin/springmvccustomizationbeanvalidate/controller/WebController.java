package idv.kevin.springmvccustomizationbeanvalidate.controller;

import idv.kevin.springmvccustomizationbeanvalidate.vo.PersonVO;
import idv.kevin.springmvccustomizationbeanvalidate.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@Slf4j
//@Validated
public class WebController {

    @GetMapping(value = "/test")
    public String testPage() {
        return "endPage";
    }

    @GetMapping("/index")
    public String helloController(Model model) {

        model.addAttribute("greeting", "The Greeting Form SpringMVC");
        model.addAttribute("subGreeting", "If see the title, means you success load data from controller");
        return "helloPage";
    }

    @GetMapping("/showForm")
    public String showForm(Model model) {
        model.addAttribute("personVO", new PersonVO());
        return "form";
    }

    @GetMapping("/showForm2")
    public String showForm2() {
        return "formViaAjax";
    }

    @PostMapping("/validate")
    public String validate(@Valid PersonVO personVO, BindingResult result) {
        log.info(personVO.toString());
        if (result.hasErrors()) {
            return "form";
        }
        return "endPage";
    }

    @PostMapping("/validateViaAjax")
    @ResponseBody
    public ResultVO validate2(@Valid PersonVO personVO, HttpServletRequest req) {
        HttpSession session = req.getSession();
        session.setAttribute("personVO", personVO);
        return new ResultVO().setResult(true).setMessage("validate success");
    }

    @GetMapping("/endPage")
    public String showEndPage(Model model, HttpServletRequest req) {
        PersonVO personVO = (PersonVO) req.getSession().getAttribute("personVO");
        model.addAttribute("personVO", personVO);
        return "endPage";
    }
}
