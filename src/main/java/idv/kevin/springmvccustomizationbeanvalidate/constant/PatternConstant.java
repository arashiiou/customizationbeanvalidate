package idv.kevin.springmvccustomizationbeanvalidate.constant;

public class PatternConstant {
    public static final String PHONE_PATTERN = "0[2-8]{1,}-?[0-9]{7,}";
    public static final String MOBILE_PATTERN = "0[0-9]{3}-?[0-9]{6}";
    public static final String BIRTHDAY_PATTERN = "yyyy-MM-dd";
    public static final String ID_PATTERN = "[a-zA-Z]{1}[1-2]{1}[0-9]{8}";
}
