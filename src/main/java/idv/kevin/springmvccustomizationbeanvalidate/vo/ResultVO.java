package idv.kevin.springmvccustomizationbeanvalidate.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class ResultVO {
    private boolean result;
    private String message;
}