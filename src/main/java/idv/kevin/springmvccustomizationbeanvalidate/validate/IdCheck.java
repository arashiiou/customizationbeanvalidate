package idv.kevin.springmvccustomizationbeanvalidate.validate;


import idv.kevin.springmvccustomizationbeanvalidate.constant.PatternConstant;
import idv.kevin.springmvccustomizationbeanvalidate.constant.StringConstant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {IdCheckValidator.class})
public @interface IdCheck {

    String message() default StringConstant.ID_CHECK_ERROR_MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String regexp() default PatternConstant.ID_PATTERN;
}



