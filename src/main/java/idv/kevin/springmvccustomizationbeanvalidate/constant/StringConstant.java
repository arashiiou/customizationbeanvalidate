package idv.kevin.springmvccustomizationbeanvalidate.constant;

public class StringConstant {
    public static final String PHONE_CHECK_ERROR_MESSAGE = "電話格式錯誤";
    public static final String MOBILE_CHECK_ERROR_MESSAGE = "手機號碼格式錯誤";
    public static final String NOT_BLANK_CHECK_ERROR_MESSAGE = "不得為空白";
    public static final String ID_CHECK_ERROR_MESSAGE = "Id 格式有誤";
}
