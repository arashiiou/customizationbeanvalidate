package idv.kevin.springmvccustomizationbeanvalidate.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IdCheckValidator implements ConstraintValidator<IdCheck, String> {

    private String regexp;

    private static String[] numberArray = {"10", "11", "12", "13", "14", "15", "16", "17", "34", "18", "19", "20", "21", "22", "35", "23", "24", "25", "26", "27", "28", "29", "32", "30", "31", "32"}; //A-Z對應的數字


    @Override
    public void initialize(IdCheck constraintAnnotation) {
        this.regexp = constraintAnnotation.regexp();
    }

    @Override
    public boolean isValid(String inputIdValue, ConstraintValidatorContext context) {
        if (inputIdValue == null) {
            return true;
        } else if (inputIdValue.matches(regexp)) {
            String engStr = "ABCDEGGHIJKLMNOPQRSTUVWXYZ";
            char engWord = inputIdValue.charAt(0);
            String engWordStr = String.valueOf(engWord).toUpperCase();
            int idx = engStr.indexOf(engWordStr);
            String finalIdStr = numberArray[idx] + inputIdValue.substring(1);
            int sum = 0;
            for (int i = 0; i < finalIdStr.length(); i++) {
                if (0 == i) {
                    sum += Integer.parseInt(finalIdStr.substring(i, i + 1));
                } else {
                    sum += Integer.parseInt(finalIdStr.substring(i, i + 1)) * (10 - i);
                }
            }
            return 10 - sum % 10 == Integer.valueOf(finalIdStr.substring(10));
        }
        return false;
    }
}
