package idv.kevin.springmvccustomizationbeanvalidate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmvccustomizationbeanvalidateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringmvccustomizationbeanvalidateApplication.class, args);
    }

}
